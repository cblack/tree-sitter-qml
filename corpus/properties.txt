============
Properties
============

Item {
  mald: 5
}

---

(program
  (object_declaration
    (qualified_identifier
      (identifier))
    (object_block
      (property_set
        (qualified_identifier
          (identifier))
        value: (number)))))

============
Attached properties
============

Item {
  Layout.alignment: 5
}

---

(program
  (object_declaration
    (qualified_identifier
      (identifier))
    (object_block
      (property_set
        (qualified_identifier
          (identifier)
          (identifier))
        value: (number)))))

============
Namespaced object properties
============

A {
    property A.B border: A.B {

    }
}

---

(program
  (object_declaration (qualified_identifier (identifier))
    (object_block
      (property_declaration
        (normal_property
          (property_declarator
            (property_type (property_type (type_identifier)) (type_identifier))
              (property_identifier))
            value: (object_declaration
                (qualified_identifier
                  (identifier) (identifier)) (object_block)))))))

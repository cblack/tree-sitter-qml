============
Pragma header
============

pragma Singleton

File {

}

---

(program
  (pragma_statement (identifier))
  (object_declaration
    (qualified_identifier (identifier))
    (object_block)))

============
Inline comment
============

File {
  component A : B {

  }
}

---

(program
  (object_declaration
    (qualified_identifier
      (identifier))
    (object_block
      (inline_type_declaration
        (identifier)
        (qualified_identifier
          (identifier))
        (object_block)))))

============
Import statements
============

import org.kde.kirigami 2.10 as Kirigami

File {

}

---

(program
  (import_statement
    (qualified_identifier (identifier) (identifier) (identifier))
    (number)
    (named_import (type_identifier)))
  (object_declaration
    (qualified_identifier (identifier))
    (object_block)))

============
Relative imports
============

import "kirigami" as Kirigami

Item {

}

---

(program
  (relative_import_statement
    (string) (named_import (type_identifier)))

  (object_declaration
    (qualified_identifier (identifier))
    (object_block)))

============
Required properties
============

Item {
  required property string mald
}

---

(program
  (object_declaration
    (qualified_identifier (identifier))
    (object_block
      (property_declaration
        (required_property
          (property_declarator (property_type (type_identifier)) (property_identifier)))))))

============
Expressions
============

Item {
  property bool mald: () => mald2
  property bool mald2: "hi"
}

---

(program
  (object_declaration
    (qualified_identifier
      (identifier))
    (object_block
      (property_declaration
        (normal_property
          (property_declarator
            (property_type
              (type_identifier))
            (property_identifier))
          (arrow_function
            (formal_parameters)
            (identifier))))
      (property_declaration
        (normal_property
          (property_declarator
            (property_type
              (type_identifier))
            (property_identifier))
          (string))))))

============
Statement bindings: block
============

Item {
  property bool mald: {
    return false
  }
}

---

(program
  (object_declaration (qualified_identifier (identifier))
  (object_block
    (property_declaration
      (normal_property
        (property_declarator (property_type (type_identifier)) (property_identifier))
        (script_statement
          (statement_block
            (return_statement (false)))))))))

============
Statement bindings: if
============

Item {
  property bool mald: if (true) {
    return false
  } else {
    return true
  }
}

---

(program
  (object_declaration (qualified_identifier (identifier))
  (object_block
    (property_declaration
      (normal_property
        (property_declarator (property_type (type_identifier)) (property_identifier))
        (script_statement
          (if_statement
            (parenthesized_expression (true))
            (statement_block (return_statement (false)))
            (else_clause (statement_block (return_statement (true)))))))))))

============
Statement bindings: with
============

Item {
  property bool mald: with (it) {
    return isGay
  }
}

---

(program
  (object_declaration (qualified_identifier (identifier))
  (object_block
    (property_declaration
      (normal_property
        (property_declarator (property_type (type_identifier)) (property_identifier))
        (script_statement
          (with_statement
            (parenthesized_expression (identifier))
            (statement_block (return_statement (identifier))))))))))

============
Statement bindings: switch
============

Item {
  property bool mald: switch (it) {
  case 0:
    return true
  default:
    return false
  }
}

---

(program
  (object_declaration (qualified_identifier (identifier))
  (object_block
    (property_declaration
      (normal_property
        (property_declarator (property_type (type_identifier)) (property_identifier))
        (script_statement
          (switch_statement (parenthesized_expression (identifier))
          (switch_body
            (switch_case (number)
              (return_statement (true)))
            (switch_default
              (return_statement (false)))))))))))


============
Statement bindings: try
============

Item {
  property bool mald: try {
    1
  } catch (err) {
    2
  }
}

---

(program
  (object_declaration (qualified_identifier (identifier))
    (object_block
      (property_declaration
        (normal_property
          (property_declarator (property_type (type_identifier)) (property_identifier))
          (script_statement
            (try_statement
              (statement_block (expression_statement (number)))
              (catch_clause (identifier)
                (statement_block (expression_statement (number)))))))))))


============
Statement bindings: function call
============

A {
  color: Qt.rgba(0)
}

---


(program
  (object_declaration
    (qualified_identifier
      (identifier))
    (object_block
      (property_set
        (qualified_identifier
          (identifier))
        (call_expression
          (member_expression
            (identifier)
            (property_identifier))
          (arguments
            (number)))))))

============
Expressions: object access bindings
============

A {
  value: object.a + object.b
}

---

(program
  (object_declaration
    (qualified_identifier
      (identifier))
    (object_block
      (property_set
        (qualified_identifier
          (identifier))
        (binary_expression
          (member_expression
            (identifier)
            (property_identifier))
          (member_expression
            (identifier)
            (property_identifier)))))))

============
Expression bindings: function call
============

A {
  a: Qt.rgba(0)
}

---

(program
  (object_declaration
    (qualified_identifier
      (identifier))
    (object_block
      (property_set
        (qualified_identifier
          (identifier))
        (call_expression
          (member_expression
            (identifier)
            (property_identifier))
          (arguments
            (number)))))))

